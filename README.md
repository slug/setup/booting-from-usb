# How to Boot from USB Drive.



You can easily find this information on the Web, specific to your computer model. But, here's some general primer:

First of all, what is booting? - All software, including operating systems, run from memory. When you turn off a computer, everything on memory is erased. Next time you turn on the computer, there is nothing on the memory, including the operating system. Booting, or bootstrapping, is essentially a process loading an operating system software onto memory so that it can operate the computer.

Booting is done through BIOS or UEFI. Traditionally, from the time of IBM XT/AT decades ago, the PCs used BIOS. More recently, this has been replaced by UEFI. If you have a relatively new computer, you will likely have UEFI. If not, most likely your computer uses BIOS.

How to open the BIOS/UEFI settings screen? - When you turn on a computer, there is a brief moment before you can see your operating system (e.g., Windows or Mac) being loaded. During that moment, you press a special key (specific to your computer). In some computer models, it displays message as to which key to press to open the settings screen. If not, you can easily find the information on the Web. Typically, it can be an Esc key, or one of the function keys on PCs, like F2, F8, F10, F11, or F12, etc. On Mac, it's the Cmd key.

Once you open the BIOS settings screen, you will need to change the "boot sequence" (the exact terms may differ from computer to computer). Move the "USB" option to the top, before the Hard Drive. (Your hard drive includes a boot loader loading your default operating system, Windows or Mac.)

In case of UEFI, there might require some additional steps, again depending on your precise computer model. The first thing you'll need to do is to disable, or turn off, "Secure Booting" option. Then, you may have to enable "legacy" booting. The bootable USB drive we ship can be booted either from UEFI mode or Legacy mode. But, you may have to try different options or tweak some settings, etc., to make it work on your particular computer.

Now, insert the install USB flash drive, and save the settings and exit. The computer will boot from the USB flash drive.

Note that, in some cases, just a generic "USB" (in the boot sequence) might not be sufficient, and you may have to pick a particular USB device (e.g., when multiple USB devices are connected to the computer). If so, you'll have to insert the installer flash drive before opening the BIOS/UEFI settings screen so that it can automatically recognize the flash drive. The installer flash drive should be in front of any other USB drives in the boot sequence.


